# OpenHarmony代码下载

## 环境准备(Ubuntu)
1. 安装工具：
```shell
sudo apt install git
sudo apt install git-lfs
sudo apt install curl
```
2. 配置git：
```shell
git config --global user.name yourname
git config --global user.email youremail
git config --global credential.helper store
```
3. 安装repo码云工具：
```shell
wget https://gitee.com/guodongchen/openharmony-doc/raw/master/tools/repo
sudo mv repo /usr/local/bin/repo
sudo chmod a+x /usr/local/bin/repo
```
4. 安装pip
```shell
sudo apt install python-pip
```
5.  下载OpenHarmony代码 (先把ssh key注册到gitee网站)
```shell
repo init -u ssh://git@gitee.com/openharmony/manifest.git -b master --no-repo-verify
repo sync -c --no-tags
repo forall -c 'git lfs pull'
```
6. 下载编译工具(如果用docker编译，这一步应该不用)
```shell
bash build/prebuilts_download.sh
```



## 参考文章
https://gitee.com/openharmony/manifest


https://gitee.com/openharmony/docs/blob/master/zh-cn/OpenHarmony-Overview_zh.md