# 编译

## 环境准备
1. 安装软件
```shell
sudo apt install python3.8
sudo apt install python3-pip
```
> python需要3.7.4以上版本。

2. 安装编译工具
配置 ~/.pip/pip.conf 文件,
```
```
安装ohos-build:
```shell
python3 -m pip install --user ohos-build
```

安装的鸿蒙构建工具(如 hb)在 ~/.local/bin 目录，需要添加到环境变量path中。


## 参考链接

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup-linux.md

