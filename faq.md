# FAQ

##### 1. Checking out projects:  15% (36/231) security_huksgit-lfs filter-process --skip: 1: git-lfs filter-process --skip: git-lfs: not found

解决办法: sudo apt install git-lfs

##### 2. Could not find a version that satisfies the requirement ohos-build (from versions: )

解决办法: python3版本太老，安装 python3.8 

##### 3. No module named 'apt_pkg'
升级python很容易遇到此问题。
解决办法：
```
sudo apt-get remove python3-apt
sudo apt-get install python3-apt
```

发现即使用上面方法也很难解决。
执行 hb 正常了，但是 执行其它命令 add-apt-repository 失败。

可以在执行其它命令失败时，把 /usr/bin/python3 指向 python3.6。



## ohters

##### 1. 安装 Android Studio
https://www.linuxidc.com/Linux/2018-12/156056.htm


