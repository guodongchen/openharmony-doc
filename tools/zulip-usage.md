# Zulip使用方法

## Zulip 下载地址

https://zulip.com/

有Windows/Linux/Android版本。

## 登录方法

- 先找人邀请加入。

- 收到邀请邮件，按邮件说明操作。
  - 如果是OpenHarmony组织，在组织URL位置输入：https://zulip.openharmony.cn
  - 然后用自己的用户名密码登录即可。

## 使用方法

- 关注感兴趣的Stream
- 一个Stream内部可以创建话题，相关讨论放到一个话题下
- 可以发私信给一个人或者多个人

